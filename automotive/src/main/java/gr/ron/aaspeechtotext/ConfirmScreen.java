/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package gr.ron.aaspeechtotext;

import static androidx.car.app.model.Action.BACK;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;
import androidx.car.app.CarContext;
import androidx.car.app.CarToast;
import androidx.car.app.Screen;
import androidx.car.app.model.ItemList;
import androidx.car.app.model.ListTemplate;
import androidx.car.app.model.Row;
import androidx.car.app.model.Template;
import androidx.lifecycle.DefaultLifecycleObserver;
import androidx.lifecycle.LifecycleOwner;
import androidx.preference.PreferenceManager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * Creates a screen that demonstrates usage of the full screen {@link ListTemplate} to display a
 * full-screen list.
 */
public final class ConfirmScreen extends Screen implements DefaultLifecycleObserver {
    ArrayList<String> result;
    int seconds;
    Handler handler = new Handler(Looper.getMainLooper());
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (seconds == 0) {
                openMap();
            } else {
                seconds--;
                CarToast.makeText(
                        getCarContext(),
                        getCarContext().getResources().getString(R.string.opening_mas_in)
                                + seconds
                                + getCarContext().getResources().getString(R.string.seconds),
                        CarToast.LENGTH_LONG).show();
                handler.postDelayed(runnable, 1000);
            }
        }
    };

    public ConfirmScreen(@NonNull CarContext carContext, ArrayList<String> result) {
        super(carContext);
        getLifecycle().addObserver(this);
        this.result = result;
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getCarContext());
        this.seconds = Integer.valueOf(preferences.getString("confirmation_delay", "5"));
        handler.postDelayed(runnable, 1000);
    }

    public void openMap() {
        handler.removeCallbacks(runnable);

        StringBuilder builder = new StringBuilder();
        builder.append("geo:0,0?q= ");
        builder.append(result.stream().map((item) -> {
            try {
                return URLEncoder.encode(item, "utf-8");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
                return "";
            }
        }).collect(Collectors.joining("+")));

        Uri gmmIntentUri = Uri.parse(builder.toString());
        Intent mapIntent = new Intent(CarContext.ACTION_NAVIGATE, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        mapIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getCarContext().startCarApp(mapIntent);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                getScreenManager().pop();
            }
        }, 5000);
    }

    @NonNull
    @Override
    public Template onGetTemplate() {
        ItemList.Builder listBuilder = new ItemList.Builder();
        listBuilder
                .addItem(new Row.Builder()
                        .setTitle(result.toString())
                        .setOnClickListener(() -> {
                            openMap();
                        })
                        .build())
                .addItem(new Row.Builder()
                        .setOnClickListener(() -> {
                            openMap();
                        })
                        .setTitle(getCarContext().getResources().getString(R.string.open_map_now))
                        .build())
                .addItem(new Row.Builder()
                        .setOnClickListener(() -> {
                            handler.removeCallbacks(runnable);
                            getScreenManager().pop();
                        })
                        .setTitle(getCarContext().getResources().getString(R.string.new_speech_recognition))
                        .build())
        ;

        return new ListTemplate.Builder()
                .setSingleList(listBuilder.build())
                .setTitle(getCarContext().getResources().getString(R.string.route_to))
                .setHeaderAction(BACK)
                .build();
    }

    @Override
    public void onStop(@NonNull LifecycleOwner owner) {
        handler.removeCallbacks(runnable);
    }
}
